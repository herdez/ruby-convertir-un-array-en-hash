## Ejercicio - Método para convertir un array en hash


Define el método `array_to_hash` que recibe un arreglo y convierte este arreglo en un hash. Todas las pruebas en el `driver code` deben ser true.


```ruby
#array_to_hash method



#Driver code

p array_to_hash([["animal", "cat"], 
                 ["name", "Carlos"],
                 ["things", "pencil"]]) == {
                                            "animal"=>"cat", 
                                            "name"=>"Carlos", 
                                            "things"=>"pencil"
                                           }
```
